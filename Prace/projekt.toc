\select@language {czech}
\contentsline {chapter}{\numberline {1}\'Uvod}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Inteligentn\IeC {\'\i } dom\'acnost}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Aktu\'aln\IeC {\'\i } stav}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Syst\'em na FIT VUT}{5}{section.2.2}
\contentsline {chapter}{\numberline {3}Koncov\'a za\v r\IeC {\'\i }zen\IeC {\'\i }}{8}{chapter.3}
\contentsline {section}{\numberline {3.1}Typy koncov\'ych prvk\r u}{8}{section.3.1}
\contentsline {section}{\numberline {3.2}Rozd\IeC {\'\i }ly emulovan\'ych prvk\r u a~hardwarov\'ych za\v r\IeC {\'\i }zen\IeC {\'\i }}{9}{section.3.2}
\contentsline {section}{\numberline {3.3}Generovan\IeC {\'\i } n\'ahodn\'ych hodnot}{9}{section.3.3}
\contentsline {chapter}{\numberline {4}Objektov\'y n\'avrh aplikace}{10}{chapter.4}
\contentsline {section}{\numberline {4.1}T\v r\IeC {\'\i }da dom\'acnosti}{10}{section.4.1}
\contentsline {section}{\numberline {4.2}T\v r\IeC {\'\i }da adapt\'eru}{10}{section.4.2}
\contentsline {section}{\numberline {4.3}Abstraktn\IeC {\'\i } t\v r\IeC {\'\i }da koncov\'eho prvku}{11}{section.4.3}
\contentsline {section}{\numberline {4.4}Abstraktn\IeC {\'\i } t\v r\IeC {\'\i }da p\v ren\'a\v sen\'ych typ\r u}{12}{section.4.4}
\contentsline {chapter}{\numberline {5}S\IeC {\'\i }\v tov\'a komunikace}{14}{chapter.5}
\contentsline {section}{\numberline {5.1}Komunika\v cn\IeC {\'\i } protokol emul\'atoru}{14}{section.5.1}
\contentsline {section}{\numberline {5.2}Implementace komunikace jednotliv\'ych koncov\'ych za\v r\IeC {\'\i }zen\IeC {\'\i }}{16}{section.5.2}
\contentsline {chapter}{\numberline {6}Grafick\'e rozhran\IeC {\'\i } aplikace}{18}{chapter.6}
\contentsline {section}{\numberline {6.1}N\'astroj Qt Creator}{18}{section.6.1}
\contentsline {section}{\numberline {6.2}N\'avrh a~implementace grafick\'eho rozhran\IeC {\'\i }}{18}{section.6.2}
\contentsline {section}{\numberline {6.3}Popis ovl\'ad\'an\IeC {\'\i }}{19}{section.6.3}
\contentsline {chapter}{\numberline {7}Testov\'an\IeC {\'\i }}{23}{chapter.7}
\contentsline {section}{\numberline {7.1}Parametry testovac\IeC {\'\i }ch prost\v red\IeC {\'\i }}{23}{section.7.1}
\contentsline {section}{\numberline {7.2}Pr\r ub\v eh testov\'an\IeC {\'\i }}{24}{section.7.2}
\contentsline {chapter}{\numberline {8}Z\'av\v er}{27}{chapter.8}
\contentsline {chapter}{\numberline {A}Diagram t\v r\IeC {\'\i }d}{29}{appendix.A}
\contentsline {chapter}{\numberline {B}V\'ystup serverov\'e aplikace}{30}{appendix.B}
