#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include <QObject>
#include <QThread>
#include <QDataStream>
#include <QHostAddress>
#include <QTimer>


#include <QTcpSocket>

#include "sensor.h"
#include "aktor.h"
#include "sensoraktor.h"


class NetworkManager : public QThread
{
    Q_OBJECT
public:
    explicit NetworkManager(Element*,QThread *parent = 0);
    ~NetworkManager();
    void sendSyn(QString domain, QHostAddress address, bool useIP,quint8 initValue = 0);
    Element* getElement();
    void disconnect();

    void readPacket(QByteArray);

    //porovnani sensoru a sensoru ulozenem v manageru
    inline bool operator == (NetworkManager &other) {return (other.getElement() == m_device);}
    
private:
     QTcpSocket *socket;
     QDataStream *stream;
     Element *m_device;
     quint8 init;

     QTimer* waitForReply;
    
public slots:
     void writePacket();
     void getData();
     void dontRead();

signals:
     void reActivated();
    
};

#endif // NETWORKMANAGER_H
