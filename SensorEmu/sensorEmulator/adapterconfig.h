#ifndef ADAPTERCONFIG_H
#define ADAPTERCONFIG_H

#include <QDialog>
#include <QHostAddress>

#include "adapter.h"

namespace Ui {
class adapterConfig;
}

class adapterConfig : public QDialog
{
    Q_OBJECT
    
public:
    adapterConfig(adapter *configured, QWidget *parent = 0); //modify adapter
    adapterConfig(QString name,QWidget *parent = 0);//new adapter
    ~adapterConfig();

signals:
    void sendValues(const QString &name,QHostAddress adapterAddress,bool,int frst,int scnd,int third,int last,QString domain);
    void deleteThis();

private slots:
    void accepted();
    void canceled();
    void changeHostSourceToIP(bool);
    void changeHostSourceToDomain(bool);
    
private:
    Ui::adapterConfig *ui;
    adapter *configuredAdapter;


};

#endif // ADAPTERCONFIG_H
