#ifndef HOME_H
#define HOME_H

#include <list>
#include <QDebug>
#include <QObject>

#include "adapter.h"
//tato trida slouzi k ukladani celku

class home: public QObject
{
    Q_OBJECT
private:
 std::list<adapter*> adapterList;

public:
    home(QObject *parent = 0);
    ~home();
    void addAdapter(adapter* newAdapter);

public slots:
    void deleteAdapter(adapter* adapterToDelete);
};

#endif // HOME_H
