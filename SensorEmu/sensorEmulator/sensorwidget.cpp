#include "sensorwidget.h"
#include "ui_sensorwidget.h"

sensorWidget::sensorWidget(Element *newSensor,QVBoxLayout *insideLayout,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::sensorWidget)
{
    ui->setupUi(this);
    assignedSensor = newSensor;

    ui->less->hide();
    connect(ui->more,SIGNAL(clicked()),this,SLOT(showExtension()));
    connect(ui->less,SIGNAL(clicked()),this,SLOT(hideExtension()));
    connect(ui->remove,SIGNAL(clicked()),this,SLOT(deleteSensor()));

    ui->name->setText(assignedSensor->getName());
    ui->type->setText(assignedSensor->getType());

    //qDebug() << newSensor->getValuesList()->size();
    connect(newSensor,SIGNAL(valuesReceived()),this,SLOT(receivedValues()));

    types = new sensorWidgetExtension();
    insideLayout->addWidget(this);
    insideLayout->addWidget(types);
    types->hide();

}

void sensorWidget::receivedValues()
{
   types->addLabel(assignedSensor->getValueAt(assignedSensor->getSize()-1));
}

sensorWidget::~sensorWidget()
{
    delete ui;
    delete types;
}

void sensorWidget::showExtension()
{
    ui->more->hide();
    ui->less->show();
    types->show();
}

void sensorWidget::hideExtension()
{
    ui->less->hide();
    ui->more->show();
    types->hide();
}

void sensorWidget::deleteSensor()
{
    emit(deleteThis(this));
}

Element* sensorWidget::checkSensor()
{
  return assignedSensor;
}
