#include "sensorconfig.h"
#include "ui_sensorconfig.h"

sensorConfig::sensorConfig(const QString sensorName,int sensorCount,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::sensorConfig)
{
    ui->setupUi(this);

    // nastavime zakladni vlastnosti okna
     setWindowTitle("Nastaveni sensoru " + sensorName);
     setMinimumWidth(400);
     setMinimumHeight(300);
     setMaximumWidth(400);
     setMaximumHeight(300);

    ui->lineEdit->setText(sensorName);
    ui->sensorRadio->setChecked(true);

    ui->scrollArea_2->setWidgetResizable(true);
    ui->scrollAreaWidgetContents_2->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
    ui->scrollAreaWidgetContents_2->setLayout(ui->insideLay);


    connect(ui->aktorRadio,SIGNAL(toggled(bool)), this, SLOT(actorChanged(bool)));
    connect(ui->sensorRadio,SIGNAL(toggled(bool)), this, SLOT(sensorChanged(bool)));
    connect(ui->sensorAktorRadio,SIGNAL(toggled(bool)), this, SLOT(saChanged(bool)));

    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accepted()));
    connect(ui->buttonBox, SIGNAL(rejected()),this, SLOT(canceled()));

    connect(ui->pushButton, SIGNAL(clicked()),this, SLOT(newType()));

    elementCount = sensorCount;

}

sensorConfig::~sensorConfig()
{
    delete ui;
}

void sensorConfig::newType()
{
    QString init;
    if (ui->aktorRadio->isChecked())
        init = "A";
    else if (ui->sensorAktorRadio->isChecked())
        init = "SA";
    else
        init = "S";

    TypeLayout *layH = new TypeLayout(init);
    connect(this,SIGNAL(changeToA()),layH,SLOT(itIsActor()));
    connect(this,SIGNAL(changeToS()),layH,SLOT(itIsSensor()));
    connect(this,SIGNAL(changeToSA()),layH,SLOT(itIsSA()));

    connect(this,SIGNAL(getValues()),layH,SLOT(accepted()));
    connect(layH,SIGNAL(ready(TypeLayout*)),this,SLOT(acceptValues(TypeLayout*)));


    ui->insideLay->addLayout(layH);

}



// tyto 3 funkce meni nastaveni velicin podle nastaveneho typu
void sensorConfig::actorChanged(bool state)
{
    if (state)
    {
        ui->spinBox->setEnabled(false);
        emit changeToA();
    }
}

void sensorConfig::sensorChanged(bool state)
{
    if (state)
    {
        ui->spinBox->setEnabled(true);
        emit changeToS();
    }
}

void sensorConfig::saChanged(bool state)
{
   if (state)
   {
       ui->spinBox->setEnabled(true);
       emit changeToSA();
   }
}


void sensorConfig::accepted()
{
    // TODO: zkontrolovat, jestli je min mensi nez max!!!

    Element *prvek = NULL;

    if (ui->aktorRadio->isChecked())
        prvek = new Aktor(elementCount,ui->lineEdit->text());
    else if (ui->sensorAktorRadio->isChecked())
        prvek = new SensorAktor(elementCount,ui->lineEdit->text());
    else
         prvek = new Sensor(elementCount,ui->lineEdit->text(),ui->spinBox->value());

    emit sendElement(prvek);

    emit getValues();


}

void sensorConfig::acceptValues(TypeLayout* vals)
{

    QString Type=vals->getComboVal();

    if (Type =="Teplota")
    {
        Temperature* teplota = new Temperature(vals->getMin(),vals->getMax());
        emit sendValues(teplota);
    }
    else if (Type == "Vlhkost")
    {
        Humidity* vlhkost = new Humidity(vals->getMin(),vals->getMax());
        emit sendValues(vlhkost);
    }
    else if (Type == "Tlak")
    {
        Presure* tlak = new Presure(vals->getMin(),vals->getMax());
        emit sendValues(tlak);
    }
    else if (Type == "Sepnuti - sensor")
    {
        SwitchSensor* sPrepinac = new SwitchSensor(vals->getOnOff());
        emit sendValues(sPrepinac);
    }
    else if (Type == "Sepnuti - prepinac")
    {
        SwitchActor* aPrepinac = new SwitchActor(vals->getOnOff());
        emit sendValues(aPrepinac);
    }
    else if (Type == "Intenzita svetla")
    {
        LightIntensity* sIntenzita = new LightIntensity(vals->getMin(),vals->getMax());
        emit sendValues(sIntenzita);
    }
    else if (Type == "Intenzita hluku")
    {
        SoundIntensity* hIntenzita = new SoundIntensity(vals->getMin(),vals->getMax());
        emit sendValues(hIntenzita);
    }
    else if (Type == "Emise")
    {
        Emision* emise = new Emision(vals->getMin(),vals->getMax());
        emit sendValues(emise);
    }
}

void sensorConfig::canceled()
{
    emit deleteThis();
}
