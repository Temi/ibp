#ifndef ADAPTER_H
#define ADAPTER_H

#include <QString>
#include <QObject>
#include <QDebug>
#include <QHostAddress>
#include <algorithm>

#include "element.h"
#include "networkmanager.h"

class adapter : public QObject
{
    Q_OBJECT

private:
    int tabIndex;
    QString name;

    QHostAddress adapterAddress;
    int firstEight;
    int secondEight;
    int thirdEight;
    int lastEight;

    QString domainName;

    bool useIP;

    std::list<Element*> sensorList;
    QList<NetworkManager*> threadList;

public:
    adapter(int index,QString name,QHostAddress adresa,int,int,int,int,bool,QString,QObject *parent = 0);
    virtual ~adapter();

    int checkIndex();
    QString getName();
    QString getDomainName();

    int getEight(int);
    bool connectedToIP();

    inline bool operator == (adapter other) {return (other.checkIndex() == this->tabIndex);}

    void deleteSensor(Element* sensorToDelete);
    void addSensor(Element* newSensor);



public slots:
    void setParams(QString newName, QHostAddress adresa);
    void setEight(int,int);
    void setConnectToIP(bool);
    void setDomainName(QString);
    void sensorWakeUp(Element*);



};

#endif // ADAPTER_H
