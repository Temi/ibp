#ifndef TYPELAYOUT_H
#define TYPELAYOUT_H

#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QCheckBox>
#include <QComboBox>
#include <QString>

class TypeLayout : public QHBoxLayout
{
    Q_OBJECT
public:
    explicit TypeLayout(QString);
    QString getComboVal();
    float getMin();
    float getMax();
    bool getOnOff();

private:
    QLabel *maxLab;
    QLabel *minLab;
    QDoubleSpinBox *min;
    QDoubleSpinBox *max;
    QCheckBox *onOff;
    QComboBox *combo;
    
signals:
    void ready(TypeLayout*);
    
public slots:
    void itIsSensor();
    void itIsActor();
    void itIsSA();

    void changeProperties(QString);

    void accepted();
    
};

#endif // TYPELAYOUT_H
