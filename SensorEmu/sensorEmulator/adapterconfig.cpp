#include "adapterconfig.h"
#include "ui_adapterconfig.h"

// tento kontruktor slouzi pro upravu jiz eistujiciho adapteru
adapterConfig::adapterConfig(adapter *configured, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::adapterConfig)
{
    ui->setupUi(this);

    //connect(ui->buttonBox->Ok,SIGNAL(clicked()),)

    // nastavime zakladni vlastnosti okna
     setWindowTitle("Nastaveni adapteru " + configured->getName());
     setMinimumWidth(400);
     setMinimumHeight(300);
     setMaximumWidth(400);
     setMaximumHeight(300);

     ui->lineEdit->setText(configured->getName());
     ui->spinBox->setValue(configured->getEight(1));
     ui->spinBox_2->setValue(configured->getEight(2));
     ui->spinBox_3->setValue(configured->getEight(3));
     ui->spinBox_4->setValue(configured->getEight(4));
     ui->lineEdit_2->setText(configured->getDomainName());

     //qDebug() << configured->connectedToIP();
     if (configured->connectedToIP())
     {
        ui->lineEdit_2->setEnabled(false);
        ui->radioButton->setChecked(true);
     }
     else
     {
         ui->spinBox->setEnabled(false);
         ui->spinBox_2->setEnabled(false);
         ui->spinBox_3->setEnabled(false);
         ui->spinBox_4->setEnabled(false);;
         ui->radioButton_2->setChecked(true);
     }

     connect(ui->radioButton_2,SIGNAL(toggled(bool)),this,SLOT(changeHostSourceToDomain(bool)));
     connect(ui->radioButton,SIGNAL(toggled(bool)),this,SLOT(changeHostSourceToIP(bool)));

     connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accepted()));
     connect(ui->buttonBox, SIGNAL(rejected()),this, SLOT(canceled()));


}

//tento kontruktor je pro novy adapter
adapterConfig::adapterConfig(QString name,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::adapterConfig)
{
    ui->setupUi(this);

    //connect(ui->buttonBox->Ok,SIGNAL(clicked()),)

    // nastavime zakladni vlastnosti okna
     setWindowTitle("Nastaveni adapteru " + name);
     setMinimumWidth(400);
     setMinimumHeight(300);
     setMaximumWidth(400);
     setMaximumHeight(300);

     ui->lineEdit->setText(name);

     //zadavat se bude IP adresa
     ui->lineEdit_2->setText("localhost");
     ui->lineEdit_2->setEnabled(false);


     connect(ui->radioButton_2,SIGNAL(toggled(bool)),this,SLOT(changeHostSourceToDomain(bool)));
     connect(ui->radioButton,SIGNAL(toggled(bool)),this,SLOT(changeHostSourceToIP(bool)));

     connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accepted()));
     connect(ui->buttonBox, SIGNAL(rejected()),this, SLOT(canceled()));

}

adapterConfig::~adapterConfig()
{
    delete ui;
}

void adapterConfig::changeHostSourceToIP(bool state)
{
    if (state)
    {
        ui->lineEdit_2->setEnabled(false);
        ui->spinBox->setEnabled(true);
        ui->spinBox_2->setEnabled(true);
        ui->spinBox_3->setEnabled(true);
        ui->spinBox_4->setEnabled(true);
    }
}

void adapterConfig::changeHostSourceToDomain(bool state)
{
    if (state)
    {
        ui->lineEdit_2->setEnabled(true);
        ui->spinBox->setEnabled(false);
        ui->spinBox_2->setEnabled(false);
        ui->spinBox_3->setEnabled(false);
        ui->spinBox_4->setEnabled(false);
    }
}

void adapterConfig::accepted() {
    QString name = ui->lineEdit->text();
    QString domainName = ui->lineEdit_2->text();

    QString adresa = ui->spinBox->text() + "." + ui->spinBox_2->text() + "." + ui->spinBox_3->text() + "." + ui->spinBox_4->text();
    // TODO kontrola adresy

    //qDebug() << adresa;

    bool connectToIP;
    if (ui->radioButton->isChecked())
        connectToIP = true;
    else
        connectToIP = false;

    int frst = ui->spinBox->text().toInt();
    int scnd = ui->spinBox_2->text().toInt();
    int third = ui->spinBox_3->text().toInt();
    int last = ui->spinBox_4->text().toInt();

    QHostAddress adapterAdresa;
    adapterAdresa.setAddress(adresa);

    emit this->sendValues(name,adapterAdresa,connectToIP,frst,scnd,third,last,domainName);
}

void adapterConfig::canceled() {
    emit this->deleteThis();
}
