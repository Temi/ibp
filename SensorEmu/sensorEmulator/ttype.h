#ifndef TTYPE_H
#define TTYPE_H

#include <QString>
#include <QObject>
#include <QDebug>

#include <qglobal.h>
#include <math.h>

class TType: public QObject
{

   Q_OBJECT
public:
    TType(QObject *p =0);
    ~TType();
    //TType(float min, float max, QObject *p = 0);
    //TType(bool, QObject *p = 0);



    virtual QString getName() = 0;
    virtual quint8 getType() = 0;
    float getMin();
    float getMax();
    float getRanVal();
    quint8 getRanBool();

    void setMin(float);
    void setMax(float);

    void update(bool newState) {state=newState;}

protected:

    float minimum;
    float maximum;
    bool state;

signals:
    void valChanged(QString);

};

#endif // TTYPE_H
