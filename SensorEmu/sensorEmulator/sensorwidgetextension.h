#ifndef SENSORWIDGETEXTENSION_H
#define SENSORWIDGETEXTENSION_H

#include <QWidget>
#include <QLabel>
#include <QGridLayout>
#include <QDebug>

#include "sensor.h"
#include "aktor.h"

namespace Ui {
class sensorWidgetExtension;
}

class sensorWidgetExtension : public QWidget
{
    Q_OBJECT
    
public:
    explicit sensorWidgetExtension(QWidget *parent = 0);
    ~sensorWidgetExtension();
    void addLabel(TType * typ);
    
private:
    Ui::sensorWidgetExtension *ui;
    QGridLayout *layout;

    int numOfRows;
    //std::list <QLabel*> labelList;


};

#endif //SENSORWIDGETEXTENSION_H
