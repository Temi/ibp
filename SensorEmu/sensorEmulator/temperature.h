#ifndef TEMPERATURE_H
#define TEMPERATURE_H

#include "ttype.h"

class Temperature : public TType
{

public:
 Temperature (float, float,QObject *parent = 0);
 QString getName();
 quint8 getType();


};

#endif // TEMPERATURE_H
