#include "tabinside.h"

tabInside::tabInside(QWidget *parent) :
    QWidget(parent)
{
    sensorCount = 0;

    //grafikcky vzhled widgetu
    layout = new QVBoxLayout();
    confAdapter = new QPushButton("Nastaveni Adapteru");
    addSensor = new QPushButton("Pridej Sensor");

    insideLayout = new QVBoxLayout();
    insideLayout->setAlignment(Qt::AlignTop);

    sensorArea = new QScrollArea();
    sensorArea->setWidgetResizable(true);

    insideWidget = new QWidget(sensorArea);
    insideWidget->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
    insideWidget->setLayout(insideLayout);

    sensorArea->setWidget(insideWidget);


    //pripojeni tlacitka nastaveni adapteru na zobrazeni okna
    connect(confAdapter,SIGNAL(clicked()),this,SLOT(showConfig()));

    //propojit tlacitko pridani sensoru
    connect(addSensor,SIGNAL(clicked()),this,SLOT(showSensorConfig()));

    //nastaveni rozlozeni
    layout->addWidget(sensorArea);
    layout->addWidget(addSensor);
    layout->addWidget(confAdapter);
    setLayout(layout);


}

//priradim k tabulatoru adapter
void tabInside::assignAdapter(adapter *newAdapter)
{
    assignedAdapter = newAdapter;
}

//slot slouzici k zobrazeni okna nastaveni
void tabInside::showConfig()
{
    configWindow = new adapterConfig(assignedAdapter);

    //propojim jeho signaly se sloty tohoto widgetu
    connect(configWindow,SIGNAL(sendValues(QString,QHostAddress,bool,int,int,int,int,QString)),this,SLOT(changeAdapter(QString,QHostAddress,bool,int,int,int,int,QString)));
    connect(configWindow,SIGNAL(deleteThis()),this,SLOT(deleteConfig()));

    configWindow->show();

}

//slot pri zmacknuti OK - smazu okno a nastavim adapteru nove parametry
//a vyslu signal ke zmene nazvu adapteru
void tabInside::changeAdapter(QString newName, QHostAddress adresa,bool useIP,int frst,int scnd,int third,int last,QString domain)
{
    deleteConfig();

    assignedAdapter->setParams(newName, adresa);
    assignedAdapter->setEight(1,frst);
    assignedAdapter->setEight(2,scnd);
    assignedAdapter->setEight(3,third);
    assignedAdapter->setEight(4,last);
    assignedAdapter->setConnectToIP(useIP);
    assignedAdapter->setDomainName(domain);

    emit this->adapterChanged(newName,assignedAdapter->checkIndex());
}

//slot pro zmacknuti Cancel - jen smazu okno
void tabInside::deleteConfig()
{
    delete configWindow;
}

void tabInside::addNewSensor(Element *prvek)
{
    sensorCount++;
    //priradime ho adapteru
     assignedAdapter->addSensor(prvek);

     //posleme nove vytvorenemu sensoru nastaveni jeho velicin
     connect(sensorConfWindow,SIGNAL(sendValues(TType*)),prvek,SLOT(receiveValues(TType*)));

     //pridam graficke zobrazeni sensoru, ulozim ho do rady a propojim jeho signal
     sensorWidget *uiPrvek = new sensorWidget(prvek,insideLayout);
     graphicSensors.push_back(uiPrvek);

     connect(uiPrvek,SIGNAL(deleteThis(sensorWidget*)),this,SLOT(deleteSensor(sensorWidget*)));


}

void tabInside::showSensorConfig()
{
    //vygenerujeme jmeno a vytvorime nove konfiguracni okno
    const QString sensorName = "Prvek" + QString::number(sensorCount);
    sensorConfWindow = new sensorConfig(sensorName,sensorCount);

    //signaly od okna nastaveni sensoru
    connect(sensorConfWindow, SIGNAL(deleteThis()),this,SLOT(deleteSensorConfig()));
    connect(sensorConfWindow, SIGNAL(sendElement(Element*)),this, SLOT(addNewSensor(Element*)));

    sensorConfWindow->show();

}

void tabInside::deleteSensorConfig()
{
    delete sensorConfWindow;
}

void tabInside::deleteSensor(sensorWidget* toDelete)
{

    graphicSensors.remove(toDelete);
    //qDebug() << "Pocet zaznamenanych graf. polozek je " << graphicSensors.size();
    assignedAdapter->deleteSensor(toDelete->checkSensor());
    delete toDelete;
}

//TODO - mazani adapteru
tabInside::~tabInside()
{
    delete insideLayout;
    delete insideWidget;

    delete confAdapter;
    delete addSensor;
    delete sensorArea;
    delete layout;

    //delete assignedAdapter;

    graphicSensors.clear();
}

int tabInside::getIndex()
{
    return assignedAdapter->checkIndex();
}

adapter* tabInside::getAdapter()
{
    return assignedAdapter;
}
