#include "sensoraktor.h"

SensorAktor::SensorAktor(int newId,QString newName,QObject *parent) : Element(newId,newName,parent){
}

QString SensorAktor::getType()
{
    return "S/A";
}

void SensorAktor::receiveTypeList(QByteArray)
{

}


void SensorAktor::activateConnection()
{
   emit activation(this);
}
