#ifndef ELEMENT_H
#define ELEMENT_H

#include <QObject>
#include <QDebug>
#include <list>
#include <QHostAddress>



#include "ttype.h"


class Element : public QObject
{
    Q_OBJECT
public:
    Element(int newId,QString newName,QObject *parent = 0);
    ~Element();

    int checkId();
    QString getName();
    virtual QString getType() = 0;

    int getSize();
    TType* getValueAt(unsigned i);
    TType * getFront();

    void connectAndSend(bool,QString,QHostAddress,quint8);

    inline bool operator == (Element *other) {return (other->checkId() == this->id);}

    int numberOfActors();
    void changeActor(int, bool);

    //void addValue(TType*);

protected:
    int id;
    QString name;



    std::vector<TType *> valuesList;



public slots:
    void receiveValues(TType* values);
    virtual void receiveActivationTime(int) {};
    virtual void activateConnection() {};
    virtual void receiveTypeList(QByteArray) {};

signals:
    void valuesReceived();
    void activation(Element*);

};

#endif // ELEMENT_H
