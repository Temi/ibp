#include "ttype.h"
TType::TType(QObject *parent):
    QObject(parent)
{

}

TType::~TType()
{

}

/*
TType::TType(float min, float max, QObject *parent):QObject(parent)
{
    minimum=min;
    maximum=max;
}

TType::TType(bool sstate, QObject *parent) :QObject(parent)
{
    state=sstate;
}
*/

float TType::getMin()
{
 return minimum;
}

float TType::getMax()
{
  return maximum;
}

float TType::getRanVal()
{
    qsrand(qrand());

    float temp = (minimum + ((float)qrand()/(float)RAND_MAX)*(maximum - minimum));
    float temp2 = floorf(temp); //prozatim zaokrouhlujeme na cele cislo! prevest na long???
    emit valChanged(QString::number(temp2));
    return temp2;

}


quint8 TType::getRanBool()
{
    qsrand(qrand());

    quint8 temp = qrand()%2;


    if (temp==1)
        emit valChanged("Zapnuto");
    else
        emit valChanged("Vypnuto");

    return temp;

}

// TODO kontrola jestli je min<max!
void TType::setMin(float newMin)
{
 minimum=newMin;
}

void TType::setMax(float newMax)
{
 maximum = newMax;
}

