#-------------------------------------------------
#
# Project created by QtCreator 2014-03-17T18:19:33
#
#-------------------------------------------------

QT       += core gui network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sensorEmulator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    adapter.cpp \
    home.cpp \
    adapterconfig.cpp \
    tabinside.cpp \
    sensorwidget.cpp \
    sensorconfig.cpp \
    ttype.cpp \
    sensorwidgetextension.cpp \
    temperature.cpp \
    presure.cpp \
    element.cpp \
    sensor.cpp \
    aktor.cpp \
    sensoraktor.cpp \
    networkmanager.cpp \
    humidity.cpp \
    switchsensor.cpp \
    switchactor.cpp \
    lightintensity.cpp \
    soundintensity.cpp \
    emision.cpp \
    typelayout.cpp

HEADERS  += mainwindow.h \
     adapter.h \
    home.h \
    adapterconfig.h \
    tabinside.h \
    sensorconfig.h \
    ttype.h \
    sensorwidget.h \
    sensorwidgetextension.h \
    temperature.h \
    presure.h \
    element.h \
    sensor.h \
    aktor.h \
    sensoraktor.h \
    networkmanager.h \
    humidity.h \
    switchsensor.h \
    switchactor.h \
    lightintensity.h \
    soundintensity.h \
    emision.h \
    typelayout.h

FORMS    += mainwindow.ui \
    adapterconfig.ui \
    sensorwidget.ui \
    sensorconfig.ui \
    sensorwidgetextension.ui
