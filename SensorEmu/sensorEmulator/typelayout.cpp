#include "typelayout.h"

TypeLayout::TypeLayout(QString Type) :
    QHBoxLayout()
{

    combo = new QComboBox();
    connect(combo,SIGNAL(currentIndexChanged(QString)),this,SLOT(changeProperties(QString)));

    onOff = new QCheckBox();
    min = new QDoubleSpinBox();
    max = new QDoubleSpinBox();
    minLab = new QLabel();
    maxLab = new QLabel();

    addWidget(combo);
    addSpacing(5);
    addWidget(onOff);
    addSpacing(5);
    addWidget(min);
    addWidget(minLab);
    addSpacing(3);
    addWidget(max);
    addWidget(maxLab);

    if (Type=="A")
    {
        itIsActor();
        onOff->show();
        min->hide();
        max->hide();
        minLab->hide();
        maxLab->hide();
    }
    else if (Type=="S")
    {
        itIsSensor();
        onOff->hide();
        min->setValue(0);
        max->setValue(0);
        max->setMinimum(-1000);
        min->setMaximum(1000);
        min->setMinimum(-1000);
        max->setMaximum(1000);
        min->show();
        max->show();
        minLab->show();
        maxLab->show();


        minLab->setText("C");
        maxLab->setText("C");
    }
    else
    {
        itIsSA();
        onOff->hide();
        min->setValue(0);
        max->setValue(0);
        max->setMinimum(-100);
        min->setMaximum(100);
        min->setMinimum(-100);
        max->setMaximum(100);
        min->show();
        max->show();
        minLab->show();
        maxLab->show();


        minLab->setText("C");
        maxLab->setText("C");
    }
}

void TypeLayout::changeProperties(QString velicina)
{
    if (velicina =="Teplota")
    {
        onOff->hide();
        min->setValue(0);
        max->setValue(0);
        max->setMinimum(-1000);
        min->setMaximum(1000);
        min->setMinimum(-1000);
        max->setMaximum(1000);
        min->show();
        max->show();
        minLab->show();
        maxLab->show();


        minLab->setText("C");
        maxLab->setText("C");
    }
    else if (velicina == "Vlhkost")
    {
        onOff->hide();
        min->setValue(0);
        max->setValue(0);
        max->setMinimum(0);
        min->setMaximum(100);
        min->setMinimum(0);
        max->setMaximum(100);

        min->show();
        max->show();
        minLab->show();
        maxLab->show();


        minLab->setText("%");
        maxLab->setText("%");
    }
    else if (velicina == "Tlak")
    {
        onOff->hide();
        min->setValue(1000);
        max->setValue(1000);
        max->setMinimum(150);
        min->setMaximum(2000);
        min->setMinimum(150);
        max->setMaximum(2000);

        min->show();
        max->show();
        minLab->show();
        maxLab->show();


        minLab->setText("hPa");
        maxLab->setText("hPa");
    }
    else if (velicina == "Sepnuti - sensor")
    {
        onOff->show();
        min->hide();
        max->hide();
        minLab->hide();
        maxLab->hide();
    }
    else if (velicina == "Sepnuti - prepinac")
    {
        onOff->show();
        min->hide();
        max->hide();
        minLab->hide();
        maxLab->hide();
    }
    else if (velicina == "Intenzita svetla")
    {
        onOff->hide();
        min->setValue(150);
        max->setValue(150);
        max->setMinimum(50);
        min->setMaximum(5000);
        min->setMinimum(50);
        max->setMaximum(5000);

        min->show();
        max->show();
        minLab->show();
        maxLab->show();


        minLab->setText("lx");
        maxLab->setText("lx");
    }
    else if (velicina == "Intenzita hluku")
    {
        onOff->hide();
        min->setValue(0);
        max->setValue(0);
        max->setMinimum(0);
        min->setMaximum(130);
        min->setMinimum(0);
        max->setMaximum(130);
        min->show();
        max->show();
        minLab->show();
        maxLab->show();


        minLab->setText("db");
        maxLab->setText("db");
    }
    else if (velicina == "Emise")
    {
        onOff->hide();
        min->setValue(0);
        max->setValue(0);
        min->setMinimum(0);
        max->setMaximum(2000);
        min->show();
        max->show();
        minLab->show();
        maxLab->show();


        minLab->setText("ppm");
        maxLab->setText("ppm");
    }
    else
    {
        delete combo;
        delete min;
        delete max;
        delete minLab;
        delete maxLab;
        delete onOff;
    }

}

void TypeLayout::itIsSA()
{
   combo->clear();
   combo->addItem("Teplota");
   combo->addItem("Vlhkost");
   combo->addItem("Tlak");
   combo->addItem("Sepnuti - sensor");
   combo->addItem("Sepnuti - prepinac");
   combo->addItem("Intenzita svetla");
   combo->addItem("Intenzita hluku");
   combo->addItem("Emise");
   combo->addItem("Smazat");

}

void TypeLayout::itIsSensor()
{
    combo->clear();
    combo->addItem("Teplota");
    combo->addItem("Vlhkost");
    combo->addItem("Tlak");
    combo->addItem("Sepnuti - sensor");
    combo->addItem("Intenzita svetla");
    combo->addItem("Intenzita hluku");
    combo->addItem("Emise");
    combo->addItem("Smazat");

}

void TypeLayout::itIsActor()
{

    combo->clear();
    combo->addItem("Sepnuti - prepinac");
    combo->addItem("Smazat");

}

QString TypeLayout::getComboVal()
{
    return combo->currentText();
}

float TypeLayout::getMin()
{
    return min->value();
}

float TypeLayout::getMax()
{
    return max->value();
}

bool TypeLayout::getOnOff()
{
    if (onOff->isChecked())
        return true;
    else
        return false;
}

void TypeLayout::accepted()
{
    emit ready(this);
}
