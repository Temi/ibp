#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    h = new home();
    connect(this,SIGNAL(deleteThisAdapter(adapter*)),h,SLOT(deleteAdapter(adapter*)));

    // pocet zalozek = adapteru je 0
    adapterCount = 0;

    // nastavime zakladni vlastnosti okna
    setWindowTitle("Emulator senzoru pro inteligentni domacnost");
    setMinimumWidth(400);
    setMinimumHeight(500);
    setMaximumWidth(400);
    setMaximumHeight(500);

    // vytvorime a nastavime komponenty do widgetu
    adapterTab = new QTabWidget;
    adapterTab->setTabsClosable(true);
    connect(adapterTab,SIGNAL(tabCloseRequested(int)),this,SLOT(setAdapter(int)));


    //robrazeni okna pri vytvareni noveho adapteru
    connect(ui->addAdapter, SIGNAL(clicked()), this, SLOT(showConfig()));

    ui->gridLayout->addWidget(adapterTab, 1, 0);

}

void MainWindow::setAdapter(int index)
{
    for (unsigned int i = 0; i < tabulators.size(); ++i) {
        if (tabulators[i]->getIndex() == index) {

            adapter* temp = tabulators[i]->getAdapter();
            emit deleteThisAdapter(temp);
            adapterTab->removeTab(index);
            return;
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;

    tabulators.clear();
    delete adapterTab;
}

/* Tato funkce pridava graficky tabulator */
void MainWindow::addAdapterTab(QString name,QHostAddress adresa,bool useIP,int frst,int scnd,int thrd,int last,QString domain)
{
    //smazu okno s nastavenim pridavani noveho tab
    deleteConfig();

    //vytvorim novy vnitrni widget do tabu a pridam ho na seznam
    tabInside *newTab = new tabInside(this);
    tabulators.push_back(newTab);
    connect(newTab,SIGNAL(adapterChanged(QString,int)),this,SLOT(setNewTabName(QString,int)));

    //vytvorim novy adapter se zadanymi parametry a priradim ho k tabu... a pote ho pridam na seznam domacnosti

    adapter *created = new adapter(adapterTab->addTab(newTab,name),name,adresa,frst,scnd,thrd,last,useIP,domain);
    newTab->assignAdapter(created);
    h->addAdapter(created);


    adapterCount++;

    //jestli byl pridan 1. adapter, smazu okno informujici o tom, ze nemam zapojene zadne adaptery
    //a pridam tab widget



}


void MainWindow::showConfig()
{
  //vygenerujeme jmeno
  const QString tabName = "Adapter" + QString::number(adapterCount);

//vytvorime nove nastavovaci okno
  newAdapter = new adapterConfig(tabName,this);

  //pripojime jeho signaly pro tlacitka OK/Cancel na sloty teto tridy
  connect(newAdapter,SIGNAL(sendValues(QString,QHostAddress,bool,int,int,int,int,QString)),this,SLOT(addAdapterTab(QString,QHostAddress,bool,int,int,int,int,QString)));
  connect(newAdapter,SIGNAL(deleteThis()),this,SLOT(deleteConfig()));

  newAdapter->show();

}

void MainWindow::deleteConfig()
{
  delete newAdapter;

}

void MainWindow::setNewTabName(QString newName, int tabIndex)
{
    adapterTab->setTabText(tabIndex,newName);
}
