#ifndef SWITCHACTOR_H
#define SWITCHACTOR_H

#include "ttype.h"

class SwitchActor : public TType
{
public:
 SwitchActor (bool,QObject *parent = 0);
 QString getName();
 quint8 getType();

};


#endif // SWITCHACTOR_H
