#include "mainwindow.h"
#include "home.h"
#include <QApplication>
#include <assert.h>

int main(int argc, char *argv[])
{
    assert("sizeof (float) == 32");

    QApplication a(argc, argv);
    a.setApplicationName("Emulator senzoru");

    MainWindow w;
    w.show();
    
    return a.exec();
}
