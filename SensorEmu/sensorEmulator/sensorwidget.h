#ifndef SENSORWIDGET_H
#define SENSORWIDGET_H

#include <QWidget>

#include "sensor.h"
#include "aktor.h"
#include "sensorwidgetextension.h"

namespace Ui {
class sensorWidget;
}

class sensorWidget : public QWidget
{
    Q_OBJECT
    
public:
    sensorWidget(Element *newSensor,QVBoxLayout *insideLayout, QWidget *parent = 0);
    ~sensorWidget();

    Element* checkSensor();
    inline bool operator == (sensorWidget* other) {return (other->checkSensor() == this->assignedSensor);}

public slots:
    void showExtension();
    void hideExtension();
    void deleteSensor();
    void receivedValues();
    
private:
    Ui::sensorWidget *ui;
    Element *assignedSensor;
    sensorWidgetExtension *types;

signals:
    void deleteThis(sensorWidget*);

};

#endif // SENSORWIDGET_H
