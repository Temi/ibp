#ifndef SENSOR_H
#define SENSOR_H

#include <QTimer>

#include "element.h"

class Sensor : public Element
{
public:
    Sensor(int,QString,int,QObject* parent=0);

    QString getType();
    void receiveActivationTime(int);
    int getInterval();

private:
    int interval;
    QTimer* activate;

public slots:
    void activateConnection();

signals:
    void paintMe();

};

#endif // SENSOR_H
