#include "sensor.h"

Sensor::Sensor(int newId,QString newName,int activation,QObject *parent) : Element(newId,newName,parent){
    interval = activation;
    qDebug()<<name<<": aktivacni cas je :"<<interval;

    activate = new QTimer(this);

    connect(activate,SIGNAL(timeout()),this,SLOT(activateConnection()));
}

QString Sensor::getType()
{
    return "S";
}

void Sensor::receiveActivationTime(int newInterval)
{
    if (newInterval != 0)
        interval = newInterval;
    // *1000 kvuli tomu, ze prijima milisekundy
    qDebug()<<name<<": aktivacni cas je :"<<interval;
    activate->setInterval(interval*1000);
    activate->start();
}

int Sensor::getInterval()
{
    return interval;
}

void Sensor::activateConnection()
{
    activate->stop();
    qDebug() << this->getName() << ": probuzen";
    emit activation(this);
}
