#ifndef TABINSIDE_H
#define TABINSIDE_H

#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QDebug>

#include "adapterconfig.h"
#include "adapter.h"
#include "sensorconfig.h"

#include "sensor.h"
#include "aktor.h"
#include "sensorwidget.h"

class tabInside : public QWidget
{
    Q_OBJECT
public:
    explicit tabInside(QWidget *parent = 0);
    ~tabInside();
    void assignAdapter(adapter *newAdapter);

    adapter* getAdapter();
    int getIndex();
    
private:
    QPushButton *confAdapter;
    QPushButton *addSensor;
    QScrollArea *sensorArea;
    QVBoxLayout *layout;
    QWidget *insideWidget;
    QVBoxLayout *insideLayout;

    adapter *assignedAdapter;

    adapterConfig *configWindow;
    int sensorCount;

    sensorConfig* sensorConfWindow;

    std::list<sensorWidget*> graphicSensors;


signals:
    void adapterChanged(QString name,int tabIndex);

public slots:
    void showConfig();
    void changeAdapter(QString newName,QHostAddress adresa,bool,int,int,int,int,QString domain);
    void deleteConfig();

    void addNewSensor(Element *prvek);
    void deleteSensor(sensorWidget* toDelete);

    void showSensorConfig();
    void deleteSensorConfig();
};

#endif // TABINSIDE_H
