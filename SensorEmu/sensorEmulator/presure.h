#ifndef PRESURE_H
#define PRESURE_H

#include "ttype.h"

class Presure : public TType
{
public:
 Presure(float,float,QObject *parent = 0);
 QString getName();
 quint8 getType();

};

#endif // PRESURE_H
