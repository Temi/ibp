#ifndef SOUNDINTENSITY_H
#define SOUNDINTENSITY_H

#include "ttype.h"

class SoundIntensity : public TType
{
public:
 SoundIntensity (float, float,QObject *parent = 0);
 QString getName();
 quint8 getType();

};


#endif // SOUNDINTENSITY_H
