#include "adapter.h"

#include <assert.h>


adapter::adapter(int index, QString newName, QHostAddress adresa, int frst, int scnd, int thrd, int last,bool connectedToIP,QString domain,QObject *parent):
    QObject(parent)
{
    tabIndex = index;
    name = newName;
    adapterAddress = adresa;
    firstEight = frst;
    secondEight = scnd;
    thirdEight = thrd;
    lastEight = last;
    useIP = connectedToIP;
    domainName = domain;

   //qDebug() << "pridavam adapter s indexem "<<index;

}

adapter::~adapter()
{
    sensorList.clear();
}

int adapter::checkIndex()
{
     return tabIndex;
}

QString adapter::getName()
{
    //qDebug() << "the name is " << name;
    return name;
}

void adapter::setParams(QString newName, QHostAddress adresa)
{
    name = newName;
    adapterAddress = adresa;
    //qDebug() << "the new name is " << name;
}

void adapter::addSensor(Element* newSensor)
{
    sensorList.push_back(newSensor);

    NetworkManager * connection = new NetworkManager(newSensor);
    threadList.push_back(connection);

    //run thread!
    connection->start();

    //connect sensor!! ...sending init value
    connection->sendSyn(domainName,adapterAddress,useIP,0xFF);
    //jestli vse probehlo v poradku, tak spojit signal s timerem...

    if (newSensor->getType() == "S")
    {
        connect(newSensor,SIGNAL(activation(Element*)),this,SLOT(sensorWakeUp(Element*)));
    }

}

void adapter::sensorWakeUp(Element* toConnect)
{
    NetworkManager equivalent(toConnect);

    NetworkManager *thread = NULL;
    for (int i = 0; i < threadList.size(); ++i) {
        if (equivalent == *(threadList[i])) {
            thread = threadList[i];
            break;
        }
    }

    assert(thread);

    //qDebug() << "Index" << toConnect->getName() << ":";


    qDebug() << thread->getElement()->getName() << ": se jde pripojit";

    thread->sendSyn(domainName,adapterAddress,useIP);

    //TODO nenajde = internal error... protoze by to nemelo nastat.


}


void adapter::deleteSensor(Element* sensorToDelete)
{
    sensorList.remove(sensorToDelete);

    // vytvorim falesny thread, ktery bude roven mazanemu threadu
    NetworkManager deleting(sensorToDelete);

    //threadList.resize(std::remove(threadList.begin(), threadList.end(), &deleting) - threadList.begin());
    threadList.removeOne(&deleting);

    delete sensorToDelete;
    //qDebug() << "Pocet zaznamenanych sensoru je " << sensorList.size();
}

void adapter::setEight(int rank,int newEight)
{
    switch (rank)
    {
        case 1: firstEight = newEight; break;
        case 2: secondEight = newEight; break;
        case 3: thirdEight = newEight; break;
        case 4: lastEight = newEight; break;
        default: break;
    }

}

int adapter::getEight(int rank)
{
    switch (rank)
    {
        case 1: return firstEight; break;
        case 2: return secondEight; break;
        case 3: return thirdEight; break;
        case 4: return lastEight; break;
        default: break;
    }

    return -1;
}

bool adapter::connectedToIP()
{
    return useIP;
}

void adapter::setConnectToIP(bool connectedToIp)
{
    useIP = connectedToIp;
}

QString adapter::getDomainName()
{
    return domainName;
}

void adapter::setDomainName(QString newName)
{
    domainName = newName;
}
