#ifndef SENSORAKTOR_H
#define SENSORAKTOR_H

#include "element.h"

class SensorAktor : public Element
{
public:
    SensorAktor(int,QString,QObject* parent=0);
    QString getType();
    void receiveTypeList(QByteArray);

public slots:
    void activateConnection();
};

#endif // SENSORAKTOR_H
