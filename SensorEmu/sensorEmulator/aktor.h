#ifndef AKTOR_H
#define AKTOR_H

#include "element.h"

class Aktor : public Element
{
public:
    Aktor(int,QString,QObject* parent = 0);
    QString getType();
    void receiveTypeList(QByteArray);
};

#endif // AKTOR_H
