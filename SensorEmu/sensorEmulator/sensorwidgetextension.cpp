#include "sensorwidgetextension.h"
#include "ui_sensorwidgetextension.h"

sensorWidgetExtension::sensorWidgetExtension(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::sensorWidgetExtension)
{
    ui->setupUi(this);
    numOfRows = 0;
    layout = new QGridLayout();
    setLayout(layout);

}

sensorWidgetExtension::~sensorWidgetExtension()
{
    //labelList.clear();
    delete layout;
    delete ui;
}

void sensorWidgetExtension::addLabel(TType * typ)
{
  QLabel *novy = new QLabel(this);
  novy->setText(typ->getName());

  QLabel *hodnota = new QLabel(this);
  connect(typ,SIGNAL(valChanged(QString)),hodnota,SLOT(setText(QString)));
  //labelList.push_back(novy);
  layout->addWidget(novy,numOfRows,0,1,1);
  layout->addWidget(hodnota,numOfRows,1,1,1);

  numOfRows++;
}

