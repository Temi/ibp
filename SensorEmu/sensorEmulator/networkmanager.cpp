#include "networkmanager.h"

NetworkManager::NetworkManager(Element* device,QThread *parent) :
    QThread(parent)
{
     waitForReply = new QTimer(this);

     //na odpoved budu cekat maximalne 10 sekund
     waitForReply->setInterval(10*1000);
     connect(waitForReply,SIGNAL(timeout()),this,SLOT(dontRead()));

     m_device = device;

     socket = new QTcpSocket (this);

     //BigEndian(network) je defaultne v QDataStream
     stream = new QDataStream(socket);
     stream->setFloatingPointPrecision(QDataStream::SinglePrecision);

     connect(socket,SIGNAL(connected()),this,SLOT(writePacket()));
     connect(this,SIGNAL(reActivated()),this,SLOT(writePacket()));

     connect(socket,SIGNAL(readyRead()),this,SLOT(getData()));

}

NetworkManager::~NetworkManager()
{
    this->disconnect();
    delete stream;
    delete socket;
}

void NetworkManager::dontRead(){

    waitForReply->stop();
    waitForReply->setInterval(10*1000);
    QObject::disconnect(this,SLOT(getData()));
}

Element* NetworkManager::getElement()
{
    return m_device;
}

void NetworkManager::sendSyn(QString domain, QHostAddress address, bool useIP,quint8 initValue)
{

    init = initValue;

    if (socket->state() != QAbstractSocket::ConnectedState)
      {
        if (useIP)
        {
            socket->connectToHost(address,7979);
        }
        else
        {
            socket->connectToHost(domain,7979);
        }
       }
    else
    {
        connect(socket,SIGNAL(readyRead()),this,SLOT(getData()));
        emit reActivated();
    }

}


//tato funkce serializuje data a posle je socketem
void NetworkManager::writePacket()
{
   if(socket->state() == QAbstractSocket::ConnectedState)
   {
    /*hlavicka
        Bity              Význam                                                                   Použitý typ
        0  - 7    :    0, inicializačná hodnota je 0xFF                                           unsigned char  1B
        8  – 23   :    Verzia komunikačného protokolu, prvá verzia bude obsahovať hodnotu 0x0001  unsigned short 2B
        24 - 39   :    Stavová informácia o stave batérie                                         unsigned short 2B
        40 - 55   :    Stavová informácia o intenzite/kvalite signálu                             unsigned short 2B
        56 - 63   :    Stavová informácia o počte prenášaných veličín, najčastejšie == 1          unsigned char  1B
        64 - 71   :    Rezervované
    */

        *stream << init;
        //qDebug() << init;


        quint16 protVersion = 1;
        *stream << protVersion;
        //qDebug() << protVersion;


        quint16 batteryQuality = 100; //TODO randomize
        *stream << batteryQuality;
        //qDebug() << batteryQuality;


        quint16 signalQuality = 100; //TODO randomize
        *stream << signalQuality;
        //qDebug() << signalQuality;


        quint8 typeCount = m_device->getSize();
        *stream << typeCount;
        //qDebug() << typeCount;


        quint8 padByte = 0;
        *stream << padByte;
        //qDebug() << padByte;


        //telo paketu podle typu (pokud neni prazdne) = aktor?
        //Ale i aktor musi nejdrive poslat sve typy :)
        if (m_device->getSize()!=0 || init==0xFF)
        {
            //qDebug() << "Posilam typy" << m_device->getSize();

            TType *current;
            for (int i=0;i<m_device->getSize();i++)
            {
                current = m_device->getValueAt(i);

                *stream << current->getType(); // vraci quint8
                //qDebug() << current->getType();

                //podminka kvuli bool typum
                if (current->getType() == 0x04 || current->getType() == 0x03)
                {
                    *stream << current->getRanBool();
                    qDebug()<< m_device->getName() << ":("<<current->getName()<<") odesila hodnotu:"<< current->getRanBool();

                }
                else {
                    *stream << current->getRanVal(); //vraci float
                    qDebug()<< m_device->getName() << ":("<<current->getName()<<") odesila hodnotu:"<< current->getRanVal();
                    //qDebug()<< current->getMax();
                }

            }
        }

        socket->flush();

        //poslano
        qDebug() << m_device->getName() << ": zprava odeslana.";
   }

}


void NetworkManager::disconnect()
{
   socket->disconnectFromHost();
}

void NetworkManager::getData ()
{
   QByteArray received;
    //qDebug() << "prijimam data";

    while(socket->bytesAvailable())
    {
        received.append(socket->readAll());
        //qDebug() << received;
    }

    readPacket(received);
}


void NetworkManager::readPacket(QByteArray received)
{
  QDataStream translate(received);

  qDebug()<<m_device->getName()<<": jde cist a spousti citac na prijeti odpovedi";
  waitForReply->start();

  quint8 control;
  quint16 newActivation = 0;

  translate >> control;
  qDebug()<<m_device->getName()<<": cte: "<<control;
  //uz cte
  waitForReply->stop();
  waitForReply->setInterval(10*1000);

  if (control==0xFF)
  {
      translate >> newActivation;
      //qDebug() << newActivation;

      if (m_device->getType()!= "A")
      {

         m_device->receiveActivationTime(newActivation);

        if (m_device->getType()=="S/A")
         qDebug() << m_device->getName() << ": posle data za: " << newActivation;
        else
         qDebug() << m_device->getName() << ": se probudi za: " << newActivation;

      }
         //qDebug() << "Jde o Aktor NEBO je aktivacni cas: "<<newActivation;

      if (m_device->getType() != "S")
      {
        //qDebug() << m_device->getType();
        quint8 type;
        bool val;


        for (int i=0;i<m_device->numberOfActors();i++)
        {
            //vyber typ
            translate>>type;
            //qDebug()<<type;
            //qDebug() << type;

            //vyber jeho hodnotu
            translate>> val;
            //qDebug()<<val;
            //qDebug() << val;


            //predej jeho hodnotu aktoru/ s/a


        }
        qDebug()<< m_device->getName() << ": prijalo:" <<type<<val;
        //m_device->receiveTypeList();


      } else QObject::disconnect(this,SLOT(getData()));


  }



}



