#include "soundintensity.h"

SoundIntensity::SoundIntensity(float min, float max,QObject *parent) : TType(parent) {

    minimum=min;
    maximum=max;
}

QString SoundIntensity::getName ()
{
    return "Intenzita hluku";
}

quint8 SoundIntensity::getType()
{
    return 0x06;
}
