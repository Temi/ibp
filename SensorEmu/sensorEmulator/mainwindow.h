#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QGridLayout>
#include <QTextEdit>
#include <QTabWidget>
#include <QDebug>
#include <list>

#include "adapter.h"
#include "home.h"
#include "tabinside.h"
#include "adapterconfig.h"

//class adapterConfig;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    home* h;
    int adapterCount;
    std::vector<tabInside*> tabulators;

    QTabWidget *adapterTab;
    adapterConfig *newAdapter;

  //  <layout class="QGridLayout" name="gridLayout">

public slots:
    void addAdapterTab(QString name,QHostAddress adresa,bool,int,int,int,int,QString);
    void showConfig();
    void deleteConfig();
    void setNewTabName(QString newName,int tabIndex);
    void setAdapter(int);

signals:
    void deleteThisAdapter(adapter*);

};

#endif // MAINWINDOW_H
