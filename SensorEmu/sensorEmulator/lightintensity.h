#ifndef LIGHTINTENSITY_H
#define LIGHTINTENSITY_H


#include "ttype.h"

class LightIntensity : public TType
{
public:
 LightIntensity (float, float,QObject *parent = 0);
 QString getName();
 quint8 getType();

};

#endif // LIGHTINTENSITY_H
