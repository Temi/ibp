#ifndef SWITCHSENSOR_H
#define SWITCHSENSOR_H


#include "ttype.h"

class SwitchSensor : public TType
{
public:
 SwitchSensor (bool,QObject *parent = 0);
 QString getName();
 quint8 getType();

};

#endif // SWITCHSENSOR_H
