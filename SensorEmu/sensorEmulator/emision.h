#ifndef EMISION_H
#define EMISION_H

#include "ttype.h"

class Emision : public TType
{
public:
 Emision (float, float,QObject *parent = 0);
 QString getName();
 quint8 getType();

};

#endif // EMISION_H
