#include "lightintensity.h"

LightIntensity::LightIntensity(float min, float max,QObject *parent) : TType(parent) {

    minimum=min;
    maximum=max;
}

QString LightIntensity::getName ()
{
    return "Intenzita svetla";
}

quint8 LightIntensity::getType()
{
    return 0x05;
}
