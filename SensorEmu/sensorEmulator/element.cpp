#include "element.h"

Element::Element(int newId,QString newName,QObject *parent) :
    QObject(parent)
{
    id = newId;
    name = newName;
}

Element::~Element()
{
  valuesList.clear();
}


int Element::checkId()
{
    return id;
}

QString Element::getName()
{
  return name;
}

void Element::receiveValues(TType* values)
{
    valuesList.push_back(values);
    emit valuesReceived();
    //qDebug() << valuesList.size() <<"Pridano "<< values->getName();
}

TType * Element::getValueAt(unsigned i)
{
    return valuesList.at(i);
}

int Element::getSize()
{
    return valuesList.size();
}

TType* Element::getFront()
{
    return valuesList.front();
}

int Element::numberOfActors()
{
    int count = 0;
    for (unsigned int i = 0; i < valuesList.size(); ++i) {
        if (valuesList[i]->getType() == 0x04) {
            count++;
        }
    }

    return count;
}

void Element::changeActor(int position,bool value)
{
    int count = 0;
    for (unsigned int i = 0; i < valuesList.size(); ++i) {
        if (valuesList[i]->getType() == 0x04) {
            count++;
            if (count==position)
            {
                valuesList[i]->update(value);
                return;
            }

        }
    }


}
