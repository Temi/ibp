#ifndef HUMIDITY_H
#define HUMIDITY_H


#include "ttype.h"

class Humidity : public TType
{
public:
 Humidity (float, float,QObject *parent = 0);
 QString getName();
 quint8 getType();

};

#endif // HUMIDITY_H
