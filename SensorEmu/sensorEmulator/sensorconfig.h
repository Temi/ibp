#ifndef SENSORCONFIG_H
#define SENSORCONFIG_H

#include <QDialog>
#include <QDebug>

#include "temperature.h"
#include "humidity.h"
#include "presure.h"
#include "switchactor.h"
#include "switchsensor.h"
#include "lightintensity.h"
#include "soundintensity.h"
#include "emision.h"

#include "aktor.h"
#include "sensor.h"
#include "sensoraktor.h"

#include "typelayout.h"


namespace Ui {
class sensorConfig;
}

class sensorConfig : public QDialog
{
    Q_OBJECT
    
public:
    explicit sensorConfig(const QString sensorName,int sensorCount,QWidget *parent = 0);
    ~sensorConfig();
    
private:
    Ui::sensorConfig *ui;
    int elementCount;

public slots:
    void actorChanged(bool state);
    void sensorChanged(bool state);
    void saChanged(bool state);

    void accepted();
    void canceled();
    void newType();

    void acceptValues(TypeLayout*);

signals:
    void deleteThis();
    void sendElement(Element*);
    void sendValues(TType*);
    void boolValues();
    void minMaxValues();

    void changeToS();
    void changeToA();
    void changeToSA();

    void getValues();
};

#endif // SENSORCONFIG_H
